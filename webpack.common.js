const webpack = require('webpack');
const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

var BUILD_DIR = path.resolve(__dirname, 'public');
var APP_DIR = path.resolve(__dirname, 'src/client/app');

var common = {
  entry: './src/index.js',

  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', "react"]
        }
      },
      {
        test: /\.css$/,
        exclude: [/node_modules/, /public/],
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({ 
          fallback:'style-loader',
          use:['css-loader', 'autoprefixer-loader']
        }))
      },
      {
        test: /\.scss$/,
        exclude: [/node_modules/, /public/],
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({ 
          fallback:'style-loader',
          use:['css-loader', 'autoprefixer-loader', 'sass-loader']
        }))
      },
      {
        test: /\.less$/,
        exclude: [/node_modules/, /public/],
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({ 
          fallback:'style-loader',
          use:['css-loader', 'autoprefixer-loader', 'less-loader']
        }))
      },
      {
        test: /\.less$/,
        exclude: [/node_modules/, /public/],
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({ 
          fallback:'style-loader',
          use:['css-loader', 'autoprefixer-loader', 'stylus-loader']
        }))
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({filename:'main.css'}),
    new OptimizeCssAssetsPlugin()
  ]
}

module.exports = common;