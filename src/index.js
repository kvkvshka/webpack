import React from 'react';
import {render} from 'react-dom';

import './index.scss';

class App extends React.Component {
  render () {
    return <p>from react</p>;
  }
}

render(<App/>, document.getElementById('app'));
